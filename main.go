package main

import (
	"fmt"
	"log"
	"net/http"
	"text/template"
	"time"

	http_router "gitlab.com/stickman_0x00/go_http_router"
)

type data struct {
	Is_logged bool
	Name      string
	Css       []string
	Threads   []*thread
	Post      thread
}

func index(response http.ResponseWriter, request *http.Request) {
	tpl := template.Must(template.ParseGlob("html/*.html"))
	data := data{
		Is_logged: false,
		Name:      "Index",
		Css: []string{
			"header",
			"thread",
		},
		Threads: dummy_threads,
	}
	tpl.ExecuteTemplate(response, "index", data)
}

func logged_get(response http.ResponseWriter, request *http.Request) {
	tpl := template.Must(template.ParseGlob("html/*.html"))
	data := data{
		Is_logged: true,
		Name:      "Logged",
		Css: []string{
			"header",
			"thread",
		},
		Threads: dummy_threads,
	}
	tpl.ExecuteTemplate(response, "index", data)
}

func logged_post(response http.ResponseWriter, request *http.Request) {
	tpl := template.Must(template.ParseGlob("html/*.html"))
	data := data{
		Is_logged: true,
		Name:      "Logged",
		Css: []string{
			"header",
			"thread",
		},
		Threads: dummy_threads,
	}

	// Voting system
	request.ParseForm()
	title := request.Form["post_id"]
	var vote uint
	if _, exist := request.Form["vote_down"]; exist {
		vote = VOTE_DOWN
	}

	if _, exist := request.Form["vote_up"]; exist {
		vote = VOTE_UP
	}
	for _, t := range data.Threads {
		if t.Title == title[0] {
			if t.Vote == vote {
				t.Vote = VOTE_NULL
				if vote == VOTE_DOWN {
					t.Votes++
					break
				}
				t.Votes--
				break
			} else if vote == VOTE_UP {
				if t.Vote == VOTE_DOWN {
					t.Votes++
				}
				t.Votes++
			} else {
				if t.Vote == VOTE_UP {
					t.Votes--
				}
				t.Votes--
			}
			t.Vote = vote
		}
	}

	tpl.ExecuteTemplate(response, "index", data)
}

func post_get(response http.ResponseWriter, request *http.Request) {
	tpl := template.Must(template.ParseGlob("html/*.html"))

	args := http_router.Args(request)

	var post thread
	for _, t := range dummy_threads {
		if t.Title == args["title"] {
			post = *t
			break
		}
	}

	if post.Title == "" {
		post.Title = "Not Found"
	}

	data := data{
		Is_logged: false,
		Name:      post.Title,
		Css: []string{
			"header",
			"post",
		},
		Post: post,
	}

	tpl.ExecuteTemplate(response, "post", data)
}

func log_in(response http.ResponseWriter, request *http.Request) {
	tpl := template.Must(template.ParseGlob("html/*.html"))
	data := data{
		Is_logged: false,
		Name:      "Log In",
		Css: []string{
			"log_in",
		},
	}
	tpl.ExecuteTemplate(response, "log_in", data)
}

func sign_in(response http.ResponseWriter, request *http.Request) {
	tpl := template.Must(template.ParseGlob("html/*.html"))
	data := data{
		Is_logged: false,
		Name:      "Sign In",
		Css: []string{
			"sign_in",
		},
	}
	tpl.ExecuteTemplate(response, "sign_in", data)
}

func css(response http.ResponseWriter, request *http.Request) {
	http.StripPrefix("/css/", http.FileServer(http.Dir("./static/css"))).ServeHTTP(response, request)
}

func img(response http.ResponseWriter, request *http.Request) {
	http.StripPrefix("/img/", http.FileServer(http.Dir("./static/img"))).ServeHTTP(response, request)
}

func fonts(response http.ResponseWriter, request *http.Request) {
	http.StripPrefix("/fonts/", http.FileServer(http.Dir("./static/fonts"))).ServeHTTP(response, request)
}

func a(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		c, err := r.Cookie("dcap")
		if err != nil {
			w.Header().Add("Set-Cookie", "dcap=asd")
			w.Header().Add("Refresh", "10")
			return
		}
		fmt.Println(c)
		next.ServeHTTP(w, r)
	})
}

func log_request(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		timer := time.Now()

		next.ServeHTTP(w, r)

		log.Println(r.URL, r.Method, time.Since(timer))
	})
}

func main() {
	// Normal
	r := http_router.New()
	r.GET("/", index)
	r.GET("/logged", logged_get)
	r.POST("/logged", logged_post)

	// Var
	r.GET("/post/{title}", post_get)
	// r.Middleware_route(a, "/post/{title}")

	// User login
	r.GET("/log_in", log_in)
	r.GET("/sign_in", sign_in)

	// Static files
	r.GET("/css/*", css)
	r.GET("/img/*", img)
	r.GET("/fonts/*", fonts)

	r.Middleware(log_request)

	fmt.Println(r)
	log.Fatalln(http.ListenAndServe("localhost:8080", r))
}
